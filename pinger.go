package main

import (
	"errors"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"time"
)

// result stores the number of requests and combined time to relay to the user.
type result struct {
	sync.Mutex
	n         int
	timeTotal time.Duration
}

var (
	e = flag.Duration("e", time.Second, "")
	d = flag.Duration("d", time.Minute*5, "")
	t = flag.Duration("t", time.Second*10, "")
	h = flag.Bool("h", false, "")

	// Use a blocking wait group so that client timeouts are caught before main exit.
	wg = sync.WaitGroup{}

	res = result{}
)

var usage = `Usage: ping [options...] <url>

Options:
  -e  Send a ping request -e every. Default is 1s.
  -d  Keeping sending ping requests for -d duration. Default is 5 minutes.
  -t  Timeout for http client. Default is 10s.
`

func main() {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, usage)
	}
	flag.Parse()

	// Display help only? Exit with non error exit code.
	if *h {
		flag.Usage()
		fmt.Println()
		os.Exit(0)
	}

	if flag.NArg() < 1 {
		errAndExit(errors.New("URL is a required argument for pinger."))
	}

	u, err := url.Parse(flag.Arg(0))
	if err != nil {
		errAndExit(err)
	}

	// Handle Ctrl C from the user.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		// sig is a ^C, handle it
		for range c {
			// Call collate explicitly to render results thus far.
			collate()
			os.Exit(1)
		}
	}()

	// Give some user feedback.
	fmt.Println(fmt.Sprintf("Pinging %s every %s for %s (max).", u, e, d))
	fmt.Println("Exit early with Ctrl C \n")

	// Defer the collation of results.
	defer collate()

	// Setup the ticker and end time.
	every := time.NewTicker(*e)
	end := time.Now().Add(*d)
	for range every.C {
		if time.Now().After(end) {
			break
		}

		// Send the ping in a go routine and add to the wait group.
		go ping(u)
		wg.Add(1)
	}

	// Block until all ping requests have completed.
	wg.Wait()
}

// collate is called at ping end to collate results for the user.
func collate() {
	res.Lock()
	defer res.Unlock()
	fmt.Println()
	fmt.Println(fmt.Sprintf("Completed %d calls with an average time of %s (combined time %s)",
		res.n,
		res.timeTotal/(time.Duration(res.n)),
		res.timeTotal,
	))
}

// errAndExit displays any errors on ping execution along with flag usage.
func errAndExit(e error) {
	if e != nil {
		fmt.Fprint(os.Stderr, e)
		fmt.Fprintf(os.Stderr, "\n\n")
	}

	flag.Usage()
	fmt.Println()
	os.Exit(1)
}

// ping does the actual request call and updates the res timer values.
func ping(u *url.URL) {
	defer wg.Done()

	// Assign a timeout on the default http client.
	http.DefaultClient.Timeout = *t
	begin := time.Now()
	r, err := http.Head(u.String())
	if err != nil {
		errAndExit(err)
	}
	elapsed := time.Now().Sub(begin)

	// We only count non error calls.
	res.Lock()
	res.n++
	res.timeTotal = res.timeTotal + elapsed
	res.Unlock()

	// Display some helpful info for the user.
	fmt.Println(fmt.Sprintf(
		"%s called with status: %d in a time of: %s",
		u,
		r.StatusCode,
		elapsed,
	))
}
