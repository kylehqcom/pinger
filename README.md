# Pinger

A simple Golang CLI tool to repeatably *ping* a request to a resource over a time period. 

To use first go get the package with `go get gitlab.com/kylehqcom/pinger` & then `cd $GOPATH/src/gitlab.com/kylehqcom/pinger`.

You can run interactively with `go run ./pinger.go` or compile with `go build ./pinger.go`.

## Usage

*pinger* takes a single required arg of a url.

Call pinger with the help flag `-h` to see available options.

```
$> go run ./pinger.go -h

Usage: ping [options...] <url>
 
Options:
  -e  Send a ping request -e every. Default is 1s.
  -d  Keeping sending ping requests for -d duration. Default is 5 minutes.
  -t  Timeout for http client. Default is 10s.
```

By default a url will be *ping'd* every second for 5 minutes. This is easily changed eg

```
$> go run ./pinger.go -d 10s -e 2s http://www.gitlab.com

Pinging http://www.gitlab.com every 2s for 10s (max).
Exit early with Ctrl C

http://www.gitlab.com called with status: 200 in a time of: 2.307403461s
http://www.gitlab.com called with status: 200 in a time of: 1.234313969s
http://www.gitlab.com called with status: 200 in a time of: 1.379548495s
http://www.gitlab.com called with status: 200 in a time of: 1.221087779s

Completed 4 calls in a total time of 6.142353704s (avg 1.535588426s)

```

If you need break early, then `Ctrl C`. The signal will be caught so that normal results are returned. Enjoy!
